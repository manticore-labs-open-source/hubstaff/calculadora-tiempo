#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 12:05:34 2020

@author: developer-1
"""

import datetime
import sys
from dateutil.rrule import DAILY, rrule, MO, TU, WE, TH, FR

def iterable_dias_de_trabajo(start_date, end_date):
  return rrule(DAILY, dtstart=start_date, until=end_date, byweekday=(MO,TU,WE,TH,FR))

print("Formato de parametros aaaa mm dd aaaa mm dd horas_diarias porcentaje_pausa_activa")
print("La primera fecha es la que empieza")
print("La segunda fecha es la que termina")

horas_semanales = 20
if(len(sys.argv) > 7):
    horas_semanales = int(sys.argv[7])

porcentaje_pausa_activa = 12.5
if(len(sys.argv) > 8):
    porcentaje_pausa_activa = float(sys.argv[8])

if(len(sys.argv) > 6):
    fecha_inicial = datetime.datetime(
        int(sys.argv[1]), 
        int(sys.argv[2]), 
        int(sys.argv[3])
        )
    fecha_final = datetime.datetime(
        int(sys.argv[4]), 
        int(sys.argv[5]), 
        int(sys.argv[6])
        )
    numero_dias = 0
    for a in iterable_dias_de_trabajo(fecha_inicial, fecha_final):
        numero_dias = numero_dias + 1
    print(f"Dias totales: {numero_dias}")
    total_horas = numero_dias * horas_semanales
    print(f"Horas totales: {total_horas}")
    horas_menos_pausa_activa = numero_dias * horas_semanales * ((100 - porcentaje_pausa_activa )/100)
    print(f"Horas totales restado pausa activas: {horas_menos_pausa_activa}")
else:
    print("No envia fechas")


    
