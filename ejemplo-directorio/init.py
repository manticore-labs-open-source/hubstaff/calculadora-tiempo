# -*- coding: utf-8 -*-
"""
Calculadora de tiempos
"""

import xlsxwriter

# Importar archivo en python
import sys
path = '/home/developer-1/Documents/Github/calculadora-tiempo'
sys.path.append(path)

import transformar
 
# Transformar los archivos y devolver lista de archivos creados
archivos = transformar.transformar_csv_a_xlsx(path)

from openpyxl import load_workbook
from openpyxl.styles import Color, PatternFill, Font, Border
from openpyxl.formatting.rule import ColorScaleRule, CellIsRule, FormulaRule

def setear_cabeceras_h_m_s_a(sheet):
    sheet['M1'].value = "Horas"
    sheet['N1'].value = "Minutos"
    sheet['O1'].value = "Segundos"
    sheet['P1'].value = "Actividad"

def setear_reporte(sheet, numero_filas):
    nombre_hoja_con_rojo = "Con horas en rojo"
    nombre_hoja_sin_rojo = "Sin horas en rojo"
    numero_fila_total = numero_filas + 2
    numero_fila_promedio = numero_filas + 1
    sheet['A1'].value = "Reporte"
    sheet['A1'].fill = PatternFill("solid", fgColor="00000080")
    sheet['A1'].font = Font(
        name='Calibri', 
        bold=True, 
        color="ffffff",
        )
    sheet['B1'].value = "Valor"
    sheet['B1'].fill = PatternFill("solid", fgColor="00000080")
    sheet['B1'].font = Font(
        name='Calibri', 
        bold=True, 
        color="ffffff",
        )
    sheet['C1'].value = "Medicion"
    sheet['C1'].fill = PatternFill("solid", fgColor="00000080")
    sheet['C1'].font = Font(
        name='Calibri', 
        bold=True, 
        color="ffffff",
        )
    rate = 2.71
    horas = 460
    if(len(sys.argv) > 1):
        if(sys.argv[1]):
            rate = sys.argv[1]
        else:
            rate = 2.71
    if(len(sys.argv) > 2):
        if(sys.argv[2]):
            horas = sys.argv[2]
        else:
            horas = 460
    sheet['A2'].value = "Horas trabajadas en Bruto"
    sheet['A3'].value = "Horas trabajadas descontando <40%"
    sheet['A4'].value = "Promedio Actividad"
    sheet['A5'].value = "Promedio Inactividad"
    sheet['A6'].value = "Horas Actividad Brutas"
    sheet['A7'].value = "Horas Inactividad Brutas"
    sheet['A8'].value = "Horas Actividad descontando 40%"
    sheet['A9'].value = "Horas Inactividad descontando 40%"
    sheet['A10'].value = "Rate"
    sheet['A11'].value = "Sueldo estimado Bruto"
    sheet['A12'].value = "Sueldo estimado descontando 40%"
    sheet['A13'].value = "Sueldo extra"
    sheet['A14'].value = "Sueldo extra descontando 40%"
    
    sheet['B2'].value = f"='{nombre_hoja_con_rojo}'!M{numero_fila_total}"
    sheet['B3'].value = f"='{nombre_hoja_sin_rojo}'!M{numero_fila_total}"
    sheet['B4'].value = f"='{nombre_hoja_sin_rojo}'!P{numero_fila_promedio}"
    sheet['B5'].value = "No aplica"
    sheet['B6'].value = "=B2*(B4/100)"
    sheet['B7'].value = "No aplica"
    sheet['B8'].value = "=B3*(B4/100)"
    sheet['B9'].value = "=VALUE(0)"
    sheet['B10'].value = f"=VALUE({rate})"
    sheet['B11'].value = "=B2*(B10)"
    sheet['B12'].value = "=B3*(B10)"
    sheet['B13'].value = f"=(B2-{horas})*B10"
    sheet['B14'].value = f"=(B3-{horas})*B10"
    
    sheet['C2'].value = "horas"
    sheet['C3'].value = "horas"
    sheet['C4'].value = "%"
    sheet['C5'].value = "%"
    sheet['C6'].value = "horas"
    sheet['C7'].value = "horas"
    sheet['C8'].value = "horas"
    sheet['C9'].value = "horas"
    sheet['C10'].value = "dólares"
    sheet['C11'].value = "dólares"
    sheet['C12'].value = "dólares"
    sheet['C13'].value = "dólares"
    sheet['C14'].value = "dólares"
    
    
    for fila in range(2, 15):
        fila_columna = 'A' + str(fila)
        sheet[fila_columna].fill = PatternFill("solid", fgColor="00993366")
        sheet[fila_columna].font = Font(
        name='Calibri', 
        bold=True, 
        color="ffffff",
        )

def establecer_valores_en_filas(longitud_filas, sheet):
    for fila in range(2, longitud_filas+1):
        sheet["M" + str(fila)].value = f"=VALUE(MID(H{fila},1,1))"
        sheet["N" + str(fila)].value = f"=VALUE(MID(H{fila},3,2))"
        sheet["O" + str(fila)].value = f"=VALUE(MID(H{fila},6,2))"
        sheet["P" + str(fila)].value = f'=VALUE(SUBSTITUTE(I{fila},"%",""))'
        fuente = Font(name='Calibri', bold=True, color="ffffff")
        sheet["P" + str(fila)].font = fuente
        sheet["P" + str(fila)].value = f'=VALUE(SUBSTITUTE(I{fila},"%",""))'

def establecer_totales(longitud_filas, sheet):
    sheet["M" + str(longitud_filas + 1)].value = f"=SUM(M2:M{longitud_filas})"
    sheet["N" + str(longitud_filas + 1)].value = f"=SUM(N2:N{longitud_filas})"
    sheet["O" + str(longitud_filas + 1)].value = f"=SUM(O2:O{longitud_filas})"
    sheet["P" + str(longitud_filas + 1)].value = f"=AVERAGE(P2:P{longitud_filas})"
    sheet["L" + str(longitud_filas + 2)].value = f"Horas totales"
    minutos = f"( VALUE(N{longitud_filas+1}) + (VALUE(O{longitud_filas+1})/60))/60"
    sheet["M" + str(longitud_filas + 2)].value = f"= VALUE(M{longitud_filas+1}) +" + minutos

def formatear_columna_actividad(sheet):
    redFill = PatternFill(start_color='EE1111', end_color='EE1111', fill_type='solid')
    sheet.conditional_formatting.add(f'P2:P{longitud_filas}',
                                     ColorScaleRule(
                                         start_type='min', 
                                         start_color='AA0000',
                                         end_type='max', 
                                         end_color='00AA00')
                                     )


for archivo in archivos:
    nombre_archivo = archivo.replace(".csv",".xlsx")
    
    wb=load_workbook(nombre_archivo)
    
    sheet = wb[wb.sheetnames[0]]
    
    longitud_filas = len(sheet["A"])
    sheet.title = "Con horas en rojo"
    
    setear_cabeceras_h_m_s_a(sheet)
    establecer_valores_en_filas(longitud_filas, sheet)
    establecer_totales(longitud_filas, sheet)
    formatear_columna_actividad(sheet)
    
    sheet_sin_rojos = wb.copy_worksheet(sheet)
    
    sheet_sin_rojos.title = "Sin horas en rojo"
    
    formatear_columna_actividad(sheet_sin_rojos)
    
    wb.create_sheet("Resumen")
    sheet_reporte = wb[wb.sheetnames[2]]
    setear_reporte(sheet_reporte, longitud_filas)
    
    wb.save(nombre_archivo)
    






