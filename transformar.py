#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 20:53:38 2020

@author: developer-1
"""


import os
import glob
import csv
from xlsxwriter.workbook import Workbook


def transformar_csv_a_xlsx(directorio):
    os.chdir(directorio)
    lista_archivos = []
    for csvfile in glob.glob(os.path.join('.', '*.csv')):
        workbook = Workbook(csvfile[:-4] + '.xlsx')
        worksheet = workbook.add_worksheet()
        print(csvfile)
        with open(csvfile, 'rt', encoding='utf8') as f:
            reader = csv.reader(f)
            for r, row in enumerate(reader):
                for c, col in enumerate(row):
                    worksheet.write(r, c, col)
        lista_archivos.append(csvfile)
        workbook.close()
    return lista_archivos